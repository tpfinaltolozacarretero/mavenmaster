//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 2 "gramatica.y"
package edu.unicen.trazas.bean.compilador;
import edu.unicen.trazas.bean.accionesSemanticas.*;
import edu.unicen.trazas.bean.compilador.*;
import edu.unicen.trazas.bean.dependencias.*;
import edu.unicen.trazas.bean.grafo.*;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.io.*;
//#line 27 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//public class ParserVal is defined in ParserVal.java


String   yytext;//user variable to return contextual strings
ParserVal yyval; //used to return semantic vals from action routines
ParserVal yylval;//the 'lval' (result) I got from yylex()
ParserVal valstk[];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
void val_init()
{
  valstk=new ParserVal[YYSTACKSIZE];
  yyval=new ParserVal();
  yylval=new ParserVal();
  valptr=-1;
}
void val_push(ParserVal val)
{
  if (valptr>=YYSTACKSIZE)
    return;
  valstk[++valptr]=val;
}
ParserVal val_pop()
{
  if (valptr<0)
    return new ParserVal();
  return valstk[valptr--];
}
void val_drop(int cnt)
{
int ptr;
  ptr=valptr-cnt;
  if (ptr<0)
    return;
  valptr = ptr;
}
ParserVal val_peek(int relative)
{
int ptr;
  ptr=valptr-relative;
  if (ptr<0)
    return new ParserVal();
  return valstk[ptr];
}
final ParserVal dup_yyval(ParserVal val)
{
  ParserVal dup = new ParserVal();
  dup.ival = val.ival;
  dup.dval = val.dval;
  dup.sval = val.sval;
  dup.obj = val.obj;
  return dup;
}
//#### end semantic value section ####
public final static short IDENTIFICATOR=257;
public final static short CONSTANT=258;
public final static short FOR=259;
public final static short DO=260;
public final static short COMP_IGUAL=261;
public final static short COMP_MAYOR_IGUAL=262;
public final static short COMP_MENOR_IGUAL=263;
public final static short COMP_DISTINTO=264;
public final static short COMP_MENOR=265;
public final static short COMP_MAYOR=266;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    1,    1,    1,    1,    3,    3,    3,    3,    3,
    5,    5,    5,    5,    8,    8,    9,    9,   10,    2,
    2,    2,    2,    2,   11,   11,   11,    4,    4,    7,
    7,    7,    7,    7,    7,    6,    6,    6,   13,   13,
   13,   12,   12,   12,   12,
};
final static short yylen[] = {                            2,
    1,    9,    5,    5,    6,    3,    3,    3,    3,    2,
    4,    4,    3,    4,    2,    1,    1,    1,    0,   12,
    2,    3,    2,    4,    4,    4,    1,    3,    3,    1,
    1,    1,    1,    1,    1,    3,    3,    1,    3,    3,
    1,    1,    4,    1,    2,
};
final static short yydefred[] = {                         0,
    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   23,
    0,    0,   44,    0,    0,    0,   41,    0,   21,    0,
    0,    4,    3,    0,    0,    0,   28,   29,   22,   33,
   34,   35,   32,   30,   31,    0,    0,   45,    0,    0,
    0,    0,    0,    0,    0,    0,   17,   18,    0,    0,
    0,    5,   24,   19,    0,    0,    0,    0,    0,    0,
    0,    0,    8,    0,   39,   40,    0,   15,   13,    0,
    0,    0,    0,   43,    0,   14,   12,   11,    0,    0,
    0,    0,    0,   25,   26,    2,    0,    0,    0,    0,
   20,
};
final static short yydgoto[] = {                          2,
   57,   58,   25,   11,   32,   26,   46,   59,   60,   81,
   68,   27,   28,
};
final static short yysindex[] = {                      -238,
  -38,    0,    0,  -12,  -48, -227,  -34,   -1,  -51,  -41,
  -22, -113, -113, -219,  -19,  -19,    8,    7,  -13,    0,
 -170,  -17,    0, -196,   10,    2,    0,    1,    0,  -55,
  -55,    0,    0, -113,   -5,   12,    0,    0,    0,    0,
    0,    0,    0,    0,    0,  -19, -191,    0, -191,  -19,
  -19,  -19,  -29,  -19,  -19,   15,    0,    0,  -47,  -55,
  -44,    0,    0,    0,   22,   27,   30,  -11,   42,   22,
    1,    1,    0,   22,    0,    0,   25,    0,    0,  -50,
 -172,  -19,  -19,    0, -173,    0,    0,    0,   -3,   38,
   39, -113, -191,    0,    0,    0,   -4,   40,  -19,   18,
    0,
};
final static short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  -42,    0,    0,    0,    0,    0,  -31,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   41,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  -39,
    0,    0,    0,    0,   44,    0,    6,    0,    0,   45,
  -20,   -9,    0,   46,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,
};
final static short yygindex[] = {                         0,
  106,  102,    0,  -25,   -2,    3,   82,    4,    0,    0,
   16,  -24,   28,
};
final static int YYTABLESIZE=268;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         42,
   42,    5,   42,   24,   42,    9,   14,   20,   88,   31,
   33,   38,    9,   38,   79,   24,   42,   35,   36,   16,
    1,   67,   36,   69,   36,   24,   15,   38,    6,   75,
   76,   62,   12,   37,   61,   37,   29,   51,   36,   52,
   34,   17,   54,   18,   51,   39,   52,   55,   65,   37,
   37,   38,   70,   63,   51,   74,   52,   90,   91,   16,
   51,   48,   52,   78,   51,   66,   52,   67,   49,   17,
   64,   18,   82,   47,   83,   15,  101,   77,   71,   72,
   80,   84,   85,   86,   89,   16,   92,   93,   98,   96,
   40,   41,   42,   43,   44,   45,   94,   95,   27,   10,
   99,  100,    9,    7,    6,    3,   10,   53,   97,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   30,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   56,    8,    0,    1,   19,   87,    0,    7,    8,    0,
    0,    0,    0,   42,   21,   22,   23,    4,   42,   42,
   42,   42,   42,   42,   38,   13,   73,   22,   23,   38,
   38,   38,   38,   38,   38,   36,    0,   22,   23,    0,
   36,   36,   36,   36,   36,   36,   37,    0,    0,    0,
    0,   37,   37,   37,   37,   37,   37,   50,    0,    0,
    0,    0,   40,   41,   42,   43,   44,   45,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         42,
   43,   40,   45,   45,   47,   61,   41,   59,   59,  123,
   13,   43,   61,   45,   59,   45,   59,   15,   16,   59,
  259,   47,   43,   49,   45,   45,   61,   59,   41,   54,
   55,   34,  260,   43,   31,   45,   59,   43,   59,   45,
  260,   43,   42,   45,   43,   59,   45,   47,   46,   59,
   43,   45,   50,   59,   43,   53,   45,   82,   83,   61,
   43,  258,   45,   60,   43,  257,   45,   93,   59,   43,
   59,   45,   43,   91,   45,   61,   59,  125,   51,   52,
  125,   93,   41,   59,  257,  125,  260,   91,   93,   92,
  261,  262,  263,  264,  265,  266,   59,   59,   93,   59,
   61,   99,   59,   59,   59,    0,    5,   26,   93,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,  256,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  256,  257,   -1,  259,  256,  256,   -1,  256,  257,   -1,
   -1,   -1,   -1,  256,  256,  257,  258,  256,  261,  262,
  263,  264,  265,  266,  256,  260,  256,  257,  258,  261,
  262,  263,  264,  265,  266,  256,   -1,  257,  258,   -1,
  261,  262,  263,  264,  265,  266,  256,   -1,   -1,   -1,
   -1,  261,  262,  263,  264,  265,  266,  256,   -1,   -1,
   -1,   -1,  261,  262,  263,  264,  265,  266,
};
}
final static short YYFINAL=2;
final static short YYMAXTOKEN=266;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,"'('","')'","'*'","'+'",null,
"'-'",null,"'/'",null,null,null,null,null,null,null,null,null,null,null,"';'",
null,"'='",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
"'['",null,"']'",null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,"'{'",null,"'}'",null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,"IDENTIFICATOR","CONSTANT","FOR","DO",
"COMP_IGUAL","COMP_MAYOR_IGUAL","COMP_MENOR_IGUAL","COMP_DISTINTO","COMP_MENOR",
"COMP_MAYOR",
};
final static String yyrule[] = {
"$accept : programa",
"programa : sent_iteracion",
"sent_iteracion : FOR '(' asignacion condicion ';' iterador ')' DO bloque_de_sentencias",
"sent_iteracion : FOR '(' error DO bloque_de_sentencias",
"sent_iteracion : FOR error ')' DO bloque_de_sentencias",
"sent_iteracion : FOR '(' error ')' DO bloque_de_sentencias",
"condicion : expresion comparador_logico expresion",
"condicion : expresion error expresion",
"condicion : expresion comparador_logico error",
"condicion : error comparador_logico expresion",
"condicion : expresion error",
"bloque_de_sentencias : '{' conjunto_de_sentencias '}' ';'",
"bloque_de_sentencias : '{' conjunto_de_sentencias '}' error",
"bloque_de_sentencias : '{' conjunto_de_sentencias ';'",
"bloque_de_sentencias : error conjunto_de_sentencias '}' ';'",
"conjunto_de_sentencias : sentencia conjunto_de_sentencias",
"conjunto_de_sentencias : sentencia",
"sentencia : sent_iteracion",
"sentencia : asignacion",
"$$1 :",
"asignacion : IDENTIFICATOR '=' expresion ';' $$1 IDENTIFICATOR '[' expresion_iterador ']' '=' expresion ';'",
"asignacion : iterador ';'",
"asignacion : '=' error ';'",
"asignacion : '=' ';'",
"asignacion : error '=' expresion ';'",
"expresion_iterador : iterador '+' factor ';'",
"expresion_iterador : iterador '-' factor ';'",
"expresion_iterador : iterador",
"iterador : IDENTIFICATOR '+' '+'",
"iterador : IDENTIFICATOR '-' '-'",
"comparador_logico : COMP_MENOR",
"comparador_logico : COMP_MAYOR",
"comparador_logico : COMP_DISTINTO",
"comparador_logico : COMP_IGUAL",
"comparador_logico : COMP_MAYOR_IGUAL",
"comparador_logico : COMP_MENOR_IGUAL",
"expresion : expresion '+' termino",
"expresion : expresion '-' termino",
"expresion : termino",
"termino : termino '*' factor",
"termino : termino '/' factor",
"termino : factor",
"factor : IDENTIFICATOR",
"factor : IDENTIFICATOR '[' expresion_iterador ']'",
"factor : CONSTANT",
"factor : '-' CONSTANT",
};

//#line 133 "gramatica.y"

//CODIGO java


AnalizadorLexico al;


public Parser(AnalizadorLexico lexico){
	this.al = lexico;
}

void yyerror(String s){

}

int yylex(){
		Token token = al.generarToken();
		if (token != null){
			yylval = new ParserVal(token);
			yylval.ival = al.getLineaActual();
			yylval.sval = token.getLexema();
			return token.getId();
		}
		return 0;
}

public void dotest(){
	yyparse();
}
//#line 337 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 2:
//#line 28 "gramatica.y"
{System.out.println("sentencia_iteracion");}
break;
case 19:
//#line 55 "gramatica.y"
{ for (Token tI : (ArrayList<Token>)val_peek(1).obj){
																										al.getTablaDependencias().agregarDependenciaRAW((Token)val_peek(3).obj, tI, al.getLineaActual());
																									}	}
break;
case 20:
//#line 58 "gramatica.y"
{ Token t = (Token) val_peek(11).obj;
																																			Token aux = (Token) val_peek(9).obj;
																																			t.setIterador(Integer.parseInt(aux.getLexema()));
																														for (Token tI : (ArrayList<Token>)val_peek(6).obj){
																														al.getTablaDependencias().agregarDependenciaRAW((Token)val_peek(11).obj, tI, al.getLineaActual());
																													}

				}
break;
case 25:
//#line 72 "gramatica.y"
{ Token aux = (Token) val_peek(1).obj;
																							 yyval = new ParserVal(Integer.parseInt(aux.getLexema())); }
break;
case 26:
//#line 74 "gramatica.y"
{ Token aux = (Token) val_peek(1).obj;
																							 yyval = new ParserVal(Integer.parseInt(aux.getLexema())); }
break;
case 27:
//#line 76 "gramatica.y"
{ Integer r = 0;
																	yyval = new ParserVal(r);}
break;
case 36:
//#line 92 "gramatica.y"
{ listaCostos r = new listaCostos();
												r.addToken(((Token)val_peek(2).obj));
												r.addToken(((Token)val_peek(0).obj));
												r.addOperador('+');
											 yyval = new ParserVal(r); }
break;
case 37:
//#line 97 "gramatica.y"
{ listaCostos r = new listaCostos();
																	r.addToken(((Token)val_peek(2).obj));
																	r.addToken(((Token)val_peek(0).obj));
																	r.addOperador('-');
																 yyval = new ParserVal(r); }
break;
case 38:
//#line 102 "gramatica.y"
{ listaCostos r = new listaCostos();
																	r.addToken(((Token)val_peek(0).obj));
																	r.addOperador('=');
																 yyval = new ParserVal(r); }
break;
case 39:
//#line 108 "gramatica.y"
{ listaCostos r = new listaCostos();
												r.addToken(((Token)val_peek(2).obj));
												r.addToken(((Token)val_peek(0).obj));
												r.addOperador('*');
											 yyval = new ParserVal(r); }
break;
case 40:
//#line 113 "gramatica.y"
{ listaCostos r = new listaCostos();
																r.addToken(((Token)val_peek(2).obj));
																r.addToken(((Token)val_peek(0).obj));
																r.addOperador('/');
															 yyval = new ParserVal(r); }
break;
case 41:
//#line 118 "gramatica.y"
{ listaCostos r = new listaCostos();
																r.addToken(((Token)val_peek(0).obj));
																r.addOperador('=');
															 yyval = new ParserVal(r); }
break;
case 42:
//#line 124 "gramatica.y"
{ yyval = val_peek(0); }
break;
case 43:
//#line 125 "gramatica.y"
{ 	Token t = (Token) val_peek(3).obj;
																											Token aux = (Token) val_peek(1).obj;
																											t.setIterador(Integer.parseInt(aux.getLexema()));}
break;
case 44:
//#line 128 "gramatica.y"
{ yyval = val_peek(0); }
break;
case 45:
//#line 129 "gramatica.y"
{ yyval = -val_peek(1); }
break;
//#line 586 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
