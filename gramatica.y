%{
package edu.unicen.trazas.bean.compilador;
import edu.unicen.trazas.bean.accionesSemanticas.*;
import edu.unicen.trazas.bean.compilador.*;
import edu.unicen.trazas.bean.dependencias.*;
import edu.unicen.trazas.bean.grafo.*;
import java.lang.Math;
import java.util.ArrayList;
import java.util.List;
import java.io.*;
%}


//Declaraciones YACC

%token IDENTIFICATOR CONSTANT FOR DO

//COMPARADORES
%token COMP_IGUAL COMP_MAYOR_IGUAL COMP_MENOR_IGUAL COMP_DISTINTO COMP_DISTINTO COMP_MENOR COMP_MAYOR


//Reglas Gramaticales

%%

programa : sent_iteracion ;

sent_iteracion 	: FOR '(' asignacion condicion ';' iterador ')' DO bloque_de_sentencias {System.out.println("sentencia_iteracion");}
							| FOR '(' error DO bloque_de_sentencias //Error sintactico, falta ')' luego de la condicion
						  	| FOR error ')' DO bloque_de_sentencias //Error sintactico, falta ')' antes de la condicion
						  	| FOR'(' error ')' DO bloque_de_sentencias //Error sintactico, falta definir la condicion
							;

condicion : expresion comparador_logico expresion
				| expresion error expresion //Error sintactico, falta definir el comparador logico
				| expresion comparador_logico error //Error sintactico, se espera una expresion luego del operador de comparacion
				| error comparador_logico expresion //Error sintactico, se espera una expresion antes del operador de comparacion
				| expresion error //Condicion definida incorrectamente
				;

bloque_de_sentencias	: '{' conjunto_de_sentencias '}' ';'
							| '{' conjunto_de_sentencias '}' error //Error sintactico, falta '.'
							| '{' conjunto_de_sentencias  ';' //Error sintactico, falta '}' luego de especificar el bloque de sentencias
							| error conjunto_de_sentencias '}' ';'//Error sintactico, falta '{' antes de especificar el bloque de sentencias
							;

conjunto_de_sentencias : sentencia conjunto_de_sentencias
							| sentencia
							;

sentencia :  sent_iteracion
				| asignacion
				;

asignacion 	: IDENTIFICATOR '=' expresion ';' { for (Token tI : (ArrayList<Token>)$3.obj){
																										al.getTablaDependencias().agregarDependenciaRAW((Token)$1.obj, tI, al.getLineaActual());
																									}	}
			  IDENTIFICATOR '[' expresion_iterador ']' '=' expresion ';' { Token t = (Token) $1.obj;
																																			Token aux = (Token) $3.obj;
																																			t.setIterador(Integer.parseInt(aux.getLexema()));
																														for (Token tI : (ArrayList<Token>)$6.obj){
																														al.getTablaDependencias().agregarDependenciaRAW((Token)$1.obj, tI, al.getLineaActual());
																													}

				}
						| iterador ';' //Que hacemos con esto al reconocer
						|  '=' error ';'
						|  '='  ';'
						|  error '=' expresion ';'
						;

expresion_iterador : iterador '+' factor ';' { Token aux = (Token) $3.obj;
																							 $$ = new ParserVal(Integer.parseInt(aux.getLexema())); }
										| iterador '-' factor ';'{ Token aux = (Token) $3.obj;
																							 $$ = new ParserVal(Integer.parseInt(aux.getLexema())); }
										| iterador { Integer r = 0;
																	$$ = new ParserVal(r);}
										;

iterador : IDENTIFICATOR '+' '+'
					| IDENTIFICATOR '-' '-'
					;

comparador_logico	: COMP_MENOR
						| COMP_MAYOR
						| COMP_DISTINTO
						| COMP_IGUAL
						| COMP_MAYOR_IGUAL
						| COMP_MENOR_IGUAL
						;

expresion : expresion '+' termino { listaCostos r = new listaCostos();
												r.addToken(((Token)$1.obj));
												r.addToken(((Token)$3.obj));
												r.addOperador('+');
											 $$ = new ParserVal(r); }
					| expresion '-' termino { listaCostos r = new listaCostos();
																	r.addToken(((Token)$1.obj));
																	r.addToken(((Token)$3.obj));
																	r.addOperador('-');
																 $$ = new ParserVal(r); }
					| termino { listaCostos r = new listaCostos();
																	r.addToken(((Token)$1.obj));
																	r.addOperador('=');
																 $$ = new ParserVal(r); }
					;

termino : termino '*' factor  { listaCostos r = new listaCostos();
												r.addToken(((Token)$1.obj));
												r.addToken(((Token)$3.obj));
												r.addOperador('*');
											 $$ = new ParserVal(r); }
				| termino '/' factor { listaCostos r = new listaCostos();
																r.addToken(((Token)$1.obj));
																r.addToken(((Token)$3.obj));
																r.addOperador('/');
															 $$ = new ParserVal(r); }
				| factor { listaCostos r = new listaCostos();
																r.addToken(((Token)$1.obj));
																r.addOperador('=');
															 $$ = new ParserVal(r); }
				;

factor	: IDENTIFICATOR { $$ = $1; }
				| IDENTIFICATOR '[' expresion_iterador ']' { 	Token t = (Token) $1.obj;
																											Token aux = (Token) $3.obj;
																											t.setIterador(Integer.parseInt(aux.getLexema()));}  //Duda si no entra en bucle y ademas ya esta reflejado en asignacion
				| CONSTANT { $$ = $1; }
				| '-' CONSTANT { $$ = $1; }
				;

%%

//CODIGO java


AnalizadorLexico al;


public Parser(AnalizadorLexico lexico){
	this.al = lexico;
}

void yyerror(String s){

}

int yylex(){
		Token token = al.generarToken();
		if (token != null){
			yylval = new ParserVal(token);
			yylval.ival = al.getLineaActual();
			yylval.sval = token.getLexema();
			return token.getId();
		}
		return 0;
}

public void dotest(){
	yyparse();
}
