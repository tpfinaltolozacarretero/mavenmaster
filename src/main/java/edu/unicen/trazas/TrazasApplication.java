package edu.unicen.trazas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //Inicializa todo lo de mvc y carga todos los controllers y servicios
public class TrazasApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrazasApplication.class, args);
	}
}
