package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;

public class ASAgregarString implements AccionSemantica{
	
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		stringActual.append(caracter); 
		return 0;
	}
}
