package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASDistintoMenorIgual implements AccionSemantica {

	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		
		if (caracter == '>') {
			Token t = new Token();
			t.setId(Token.COMP_DISTINTO);
			al.setToken(t);
			return Token.COMP_DISTINTO; 
		}
		else 
		{
			Token t = new Token(); 
			t.setId(Token.COMP_MENOR_IGUAL); 
			al.setToken(t);
			return Token.COMP_MENOR_IGUAL; 
		}
	}

}
