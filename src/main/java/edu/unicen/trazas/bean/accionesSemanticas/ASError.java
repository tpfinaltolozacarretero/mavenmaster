package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;

public class ASError implements AccionSemantica{

	@Override
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		al.agregarError("Error Lexico" + " Linea: " + al.getLineaActual()); 
		return -2; //Retorna -2 para poder identificar el error. 
	} 

}
