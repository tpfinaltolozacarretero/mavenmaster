package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASFloat implements AccionSemantica {

	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		al.retrocederIterator();
		String aux = stringActual.toString(); 
		char [] chars = new char[15]; 
		aux.getChars(0, aux.length(), chars, 0); //Conversion String a un array de bytes
		
		for (int i = 0; i < chars.length; i++){
			if (chars[i] == ','){
				chars[i] = '.'; //Reemplazo , por . oara que pueda ser reconocido por float.
				break; 
			}
		}
		
		aux = String.valueOf(chars); 
		float valor = Float.parseFloat(aux);
		
		//FALTA AGREGAR A LA TABLA
		
		if (((valor < 3.4E38) && (valor > 1.5E-38)) || (valor == 0.0) ){
			Token t = new Token(); 
			t.setId(Token.FLOAT);
			t.setLexema(""+valor);
			al.setToken(t);
			return Token.FLOAT; 
		}
		else{
			al.addError("Error constante floar fuera de rango, Linea: " + al.getLineaActual() );
			return -2; //Error lexico del float. ya que se encuentra fuera de rango 
		}
	}

}
