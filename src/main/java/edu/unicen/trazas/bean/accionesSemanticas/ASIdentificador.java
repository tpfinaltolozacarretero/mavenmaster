package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.*;

public class ASIdentificador implements AccionSemantica {
	
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		if (caracter != '\n')
			al.retrocederIterator(); 
		if (stringActual.length() > 25){
			//Posible control de longitud de identificador ? 
		}
		
		Short idReservada = al.esPalabraReservada(stringActual.toString()); 
		Token aux = new Token(); 
		if (idReservada != null){ //Es Palabra Reservada
			aux.setId(Token.IDENTIFICATOR);
			aux.setLexema("_" + stringActual.toString()); 
			al.setToken(aux);
			return Token.IDENTIFICATOR;			
		}
		else {
			Token tDefinido = al.getTablaDeSimbolos().existeIndice(stringActual.toString());
			if (tDefinido == null){ //Nuevo identificador
				aux.setId(Token.IDENTIFICATOR);
				aux.setLexema("_" + stringActual.toString());
				al.setToken(aux); //Set Token
				al.getTablaDeSimbolos().agregarVariable(aux);
			}
			else { //La variable ya fue identificada y declarada por el parser en la tabla de simbolos  
				aux.setId(tDefinido.getId());
				aux.setLexema(tDefinido.getLexema());
				al.setToken(aux);
			}
			return Token.IDENTIFICATOR; 
			
		}

		
	}
	

}
