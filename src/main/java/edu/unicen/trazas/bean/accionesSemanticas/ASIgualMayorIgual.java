package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASIgualMayorIgual implements AccionSemantica{

	
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		al.retrocederIterator();
		Character cAnterior = al.getCaracterEntrada(); 
		al.avanzarIterator();
		if (cAnterior.charValue() == '>'){
			Token aux = new Token(); 
			aux.setIndice(cAnterior.toString());
			aux.setLexema(cAnterior.toString());
			aux.setId(Token.COMP_MAYOR_IGUAL);
			return Token.COMP_MAYOR_IGUAL; 
		}
		else {
			Token aux = new Token(); 
			aux.setIndice(cAnterior.toString());
			aux.setId(Token.COMP_IGUAL);
			aux.setLexema(cAnterior.toString());
			return Token.COMP_IGUAL; 
		}
	}
	


}
