package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASIgualOMayor implements AccionSemantica {

	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		al.retrocederIterator();
		Character cAnterior = al.getCaracterEntrada(); 
		if (cAnterior.charValue() == '='){
			Token t = new Token(); 
			t.setId(Token.COMP_IGUAL);
			t.setIndice(cAnterior.toString());
			t.setLexema(cAnterior.toString());
			return Token.COMP_IGUAL; 
		} 
		else {
			Token t = new Token(); 
			t.setId(Token.COMP_MAYOR);
			t.setIndice(cAnterior.toString());
			t.setLexema(cAnterior.toString());
			return Token.COMP_MAYOR; 
		}
	}

}
