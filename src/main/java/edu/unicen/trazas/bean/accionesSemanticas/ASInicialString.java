package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;

public class ASInicialString implements AccionSemantica{

	@Override
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		stringActual.append(""); //Se resetea el stringActual
		stringActual.append(caracter); 		
		return 0;
	}

}
