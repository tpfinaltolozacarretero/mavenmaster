package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASInteger implements AccionSemantica {

	@Override
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		al.retrocederIterator();
		String aux = stringActual.toString(); 
		int i = Integer.parseInt(aux);
		Token t = new Token(); 
		t.setId(Token.INTEGER);
		t.setLexema(""+i);
		al.setToken(t);	
		//Agregar int i a la tabla.
		
		return Token.INTEGER; 
	}
	

}
