package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASMenor implements AccionSemantica {

	
	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		al.retrocederIterator();//Se retrocede el iterador para no perder el caracter leido distinto a {>, =}
		Character cAnterior = al.getCaracterEntrada(); 
		Token aux = new Token(); 
		aux.setIndice(cAnterior.toString());
		aux.setLexema(cAnterior.toString());
		aux.setId(Token.COMP_MENOR);
		al.setToken(aux);
		return Token.COMP_MENOR; 
	}

}
