package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;
import edu.unicen.trazas.bean.compilador.Token;

public class ASOperadores implements AccionSemantica {
	

	public int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter) {
		Token t = new Token(); 
		switch (caracter) {
		case '+': {
			t.setId((short) 43);
			al.setToken(t);
			return 43; // Token id para +
		}
		case '*': {
			t.setId((short) 42);
			al.setToken(t);
			return 42; // Token id para *
		}
		case '-': {
			t.setId((short) 45);
			al.setToken(t);
			return 45; // Token id para -
		}
		case '/': {
			t.setId((short) 47);
			al.setToken(t);
			return 47; // Token id para /
		}
		case '(': {
			t.setId((short) 40);
			al.setToken(t);
			return 40; // Token id para (
		}
		case ')': {
			t.setId((short) 41);
			al.setToken(t);
			return 41; // Token id para )
		}
		case ':': {
			t.setId((short) 58);
			al.setToken(t);
			return 58; // Token id para :
		}
		case '.': {
			t.setId((short) 46);
			al.setToken(t);
			return 46; // Token id para .
		}
		case '{': {
			t.setId((short) 123);
			al.setToken(t);
			return 123; // Token id para {
		}
		case '}': {
			t.setId((short) 125);
			al.setToken(t);
			return 125; // Token id para }
		}
		
		}
		return -3; //Caso invalido. 
	}
}
