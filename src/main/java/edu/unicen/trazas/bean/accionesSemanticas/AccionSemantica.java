package edu.unicen.trazas.bean.accionesSemanticas;

import edu.unicen.trazas.bean.compilador.AnalizadorLexico;

public interface AccionSemantica {

	public abstract int ejecutarAccion(AnalizadorLexico al, StringBuffer stringActual, char caracter); 
}
