package edu.unicen.trazas.bean.compilador;

import java.util.ArrayList;
import java.util.HashMap;

import edu.unicen.trazas.bean.accionesSemanticas.*;
import edu.unicen.trazas.bean.dependencias.*;
import javafx.scene.control.Tab;

public class AnalizadorLexico {
	private String entrada;
	private HashMap<String, Integer> matrizTE = new HashMap<>(); 
	private HashMap<String, AccionSemantica> matrizAS = new HashMap<>(); 
	private Integer estadoActual = 0; 
	private StringBuffer stringActual = new StringBuffer(); 
	private int iterator = 0; 
	private Character caracterActual = null; 
	private int lineaActual = 0; 
	private HashMap<String, Short> palabrasReservadas = new HashMap<>(); 
	private ArrayList<Token> listaTokens = new ArrayList<>();  
	private ArrayList<String> listaComentarios = new ArrayList<>(); 
	private Token token = null; 
	private ArrayList<String> listaErrores = new ArrayList<>(); 
	
	
	private TablaDeSimbolos tabla = new TablaDeSimbolos(); 
	private TablaDeDependencias tablaDependencias = new TablaDeDependencias(); 
	
	private AccionSemantica AS1 = new ASVacia(); 
	private AccionSemantica AS2 = new ASError(); 
	private AccionSemantica AS3 = new ASInicialString(); 
	private AccionSemantica AS4 = new ASAgregarString(); 
	private AccionSemantica AS5 = new ASDistintoMenorIgual(); 
	private AccionSemantica AS6 = new ASMenor(); 
	private AccionSemantica AS7 = new ASIgualMayorIgual(); 
	private AccionSemantica AS8 = new ASIgualOMayor(); 
	private AccionSemantica AS9 = new ASFloat(); 
	private AccionSemantica AS10 = new ASInteger(); 
	private AccionSemantica AS11 = new ASOperadores(); 
	private AccionSemantica AS12 = new ASIdentificador(); 
 	private AccionSemantica AS13 = new ASComentario(); 
	
	public AnalizadorLexico(String entrada) {
		this.setEntrada(entrada);		
		// Inicializamos lista palabra reservadas
		palabrasReservadas.put("Integer", Token.INTEGER); 
		palabrasReservadas.put("Float", Token.FLOAT);
		palabrasReservadas.put("for", Token.FOR);
	}
	
	public TablaDeDependencias getTablaDependencias(){
		return tablaDependencias; 
	}
	
	public Character getCaracterEntrada(){
		return entrada.charAt(iterator); 
	}	
	
	
	
	public void retrocederIterator(){
		iterator--; 
	}
	
	public TablaDeSimbolos getTablaDeSimbolos(){
		return tabla; 
	}
	
	public void avanzarIterator(){
		iterator++;
	}

	public String getEntrada() {
		return entrada;
	}

	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	
	public void setToken(Token t){
		this.token = t; 
	}
	
	public void addToken(Token t){
		listaTokens.add(t); 
	}
	
	public void addError(String error){
		listaErrores.add(error); 
	}
	
	private void inicializarMatrizTE(){
		matrizTE.put("0letra", 5);    matrizTE.put("0digito", 1);	 matrizTE.put("0/", 6);		matrizTE.put("0,", -1);  matrizTE.put("0+", -1);  matrizTE.put("0*():{}", -1);  matrizTE.put("0=", 3);    matrizTE.put("0-", -1);  matrizTE.put("0>", 3);     matrizTE.put("0<", 4);    matrizTE.put("0_", -1);  matrizTE.put("0otro", -2);  matrizTE.put("0blanco", 0);    matrizTE.put("0saltotab", 0);     matrizTE.put("0.", -1);
		matrizTE.put("1letra", -1);    matrizTE.put("1digito", 1);	 matrizTE.put("1/", -1);	matrizTE.put("1,", 2);   matrizTE.put("1+", -1);  matrizTE.put("1*():{}", -1);  matrizTE.put("1=", -1);   matrizTE.put("1-", -1);  matrizTE.put("1>", -1);    matrizTE.put("1<", -1);   matrizTE.put("1_", -1);  matrizTE.put("1otro", -2);  matrizTE.put("1blanco", -1);   matrizTE.put("1saltotab", -1);    matrizTE.put("1.", -1);
		matrizTE.put("2letra", -1);    matrizTE.put("2digito", 2);	 matrizTE.put("2/", -1);	matrizTE.put("2,", -1);  matrizTE.put("2+", -1);  matrizTE.put("2*():{}", -1);  matrizTE.put("2=", -1);   matrizTE.put("2-", -1);  matrizTE.put("2>", -1);    matrizTE.put("2<", -1);   matrizTE.put("2_", -1);  matrizTE.put("2otro", -2);  matrizTE.put("2blanco", -1);   matrizTE.put("2saltotab", -1);    matrizTE.put("2.", -1);
		matrizTE.put("3letra", -1);    matrizTE.put("3digito", -1);	 matrizTE.put("3/", -1);	matrizTE.put("3,", -1);  matrizTE.put("3+", -1);  matrizTE.put("3*():{}", -1);  matrizTE.put("3=", -1);   matrizTE.put("3-", -1);  matrizTE.put("3>", -1);    matrizTE.put("3<", -1);   matrizTE.put("3_", -1);  matrizTE.put("3otro", -2);  matrizTE.put("3blanco", -1);   matrizTE.put("3saltotab", -1);    matrizTE.put("3.", -1);
		matrizTE.put("4letra", -1);    matrizTE.put("4digito", -1);	 matrizTE.put("4/", -1);	matrizTE.put("4,", -1);  matrizTE.put("4+", -1);  matrizTE.put("4*():{}", -1);  matrizTE.put("4=", -1);   matrizTE.put("4-", -1);  matrizTE.put("4>", -1);    matrizTE.put("4<", -1);   matrizTE.put("4_", -1);  matrizTE.put("4otro", -2);  matrizTE.put("4blanco", -1);   matrizTE.put("4saltotab", -1);    matrizTE.put("4.", -1);
		matrizTE.put("5letra", 5);    matrizTE.put("5digito", 5);	 matrizTE.put("5/", -1);	matrizTE.put("5,", -1);  matrizTE.put("5+", -1);  matrizTE.put("5*():{}", -1);  matrizTE.put("5=", -1);   matrizTE.put("5-", -1);  matrizTE.put("5>", -1);    matrizTE.put("5<", -1);   matrizTE.put("5_", 5);   matrizTE.put("5otro", -2);  matrizTE.put("5blanco", -1);   matrizTE.put("5saltotab", -1);    matrizTE.put("5.", -1);
		matrizTE.put("6letra", -1);    matrizTE.put("6digito", -1);	 matrizTE.put("6/", 6);		matrizTE.put("6,", -1);  matrizTE.put("6+", -1);  matrizTE.put("6*():{}", -1);  matrizTE.put("6=", -1);   matrizTE.put("6-", -1);  matrizTE.put("6>", -1);    matrizTE.put("6<", -1);   matrizTE.put("6_", -1);  matrizTE.put("6otro", -2);  matrizTE.put("6blanco", -1);   matrizTE.put("6saltotab", -1);    matrizTE.put("6.", -1);
		matrizTE.put("7letra", 7);    matrizTE.put("7digito", 7);	 matrizTE.put("7/", 7);		matrizTE.put("7,", 7);   matrizTE.put("7+", 7);   matrizTE.put("7*():{}", 7);   matrizTE.put("7=", 7);    matrizTE.put("7-", 7);   matrizTE.put("7>", 7);     matrizTE.put("7<", 7);    matrizTE.put("7_", 7);   matrizTE.put("7otro", -2);  matrizTE.put("7blanco", 7);    matrizTE.put("7saltotab", -1);    matrizTE.put("7.", -1);
	}
	

	private void inicializarMatrizAS(){
		matrizAS.put("0letra", AS3);     matrizAS.put("0digito", AS3);	 	matrizAS.put("0/", AS3);	matrizAS.put("0,", AS11);  matrizAS.put("0+", AS11);  matrizAS.put("0*():{}", AS11);  matrizAS.put("0=", AS1);    matrizAS.put("0-", AS11);  matrizAS.put("0>", AS1);     matrizAS.put("0<", AS1);     matrizAS.put("0_", AS2);   matrizAS.put("0otro", AS2);  matrizAS.put("0blanco", AS1);    matrizAS.put("0saltotab", AS1);     matrizAS.put("0.", AS11);
		matrizAS.put("1letra", AS10);    matrizAS.put("1digito", AS4);	 	matrizAS.put("1/", AS10);	matrizAS.put("1,", AS4);   matrizAS.put("1+", AS10);  matrizAS.put("1*():{}", AS10);  matrizAS.put("1=", AS10);   matrizAS.put("1-", AS10);  matrizAS.put("1>", AS10);    matrizAS.put("1<", AS10);    matrizAS.put("1_", AS10);  matrizAS.put("1otro", AS2);  matrizAS.put("1blanco", AS10);   matrizAS.put("1saltotab", AS10);    matrizAS.put("1.", AS10);
		matrizAS.put("2letra", AS10);    matrizAS.put("2digito", AS4);	 	matrizAS.put("2/", AS10);	matrizAS.put("2,", AS10);  matrizAS.put("2+", AS10);  matrizAS.put("2*():{}", AS10);  matrizAS.put("2=", AS10);   matrizAS.put("2-", AS10);  matrizAS.put("2>", AS10);    matrizAS.put("2<", AS10);    matrizAS.put("2_", AS10);  matrizAS.put("2otro", AS2);  matrizAS.put("2blanco", AS10);   matrizAS.put("2saltotab", AS10);    matrizAS.put("2.", AS10);
		matrizAS.put("3letra", AS8);     matrizAS.put("3digito", AS8);	 	matrizAS.put("3/", AS8);	matrizAS.put("3,", AS8);   matrizAS.put("3+", AS8);   matrizAS.put("3*():{}", AS8);   matrizAS.put("3=", AS7);    matrizAS.put("3-", AS8);   matrizAS.put("3>", AS8);     matrizAS.put("3<", AS8);     matrizAS.put("3_", AS8);   matrizAS.put("3otro", AS2);  matrizAS.put("3blanco", AS8);    matrizAS.put("3saltotab", AS8);     matrizAS.put("3.", AS8);
		matrizAS.put("4letra", AS6);     matrizAS.put("4digito", AS6);	 	matrizAS.put("4/", AS6);	matrizAS.put("4,", AS6);   matrizAS.put("4+", AS6);   matrizAS.put("4*():{}", AS6);   matrizAS.put("4=", AS5);    matrizAS.put("4-", AS6);   matrizAS.put("4>", AS5);     matrizAS.put("4<", AS5);     matrizAS.put("4_", AS6);   matrizAS.put("4otro", AS2);  matrizAS.put("4blanco", AS6);    matrizAS.put("4saltotab", AS6);     matrizAS.put("4.", AS6);
		matrizAS.put("5letra", AS4);     matrizAS.put("5digito", AS4);	 	matrizAS.put("5/", AS12);	matrizAS.put("5,", AS12);  matrizAS.put("5+", AS12);  matrizAS.put("5*():{}", AS12);  matrizAS.put("5=", AS12);   matrizAS.put("5-", AS12);  matrizAS.put("5>", AS12);    matrizAS.put("5<", AS12);    matrizAS.put("5_", AS4);   matrizAS.put("5otro", AS2);  matrizAS.put("5blanco", AS12);   matrizAS.put("5saltotab", AS12);    matrizAS.put("5.", AS12);
		matrizAS.put("6letra", AS11);    matrizAS.put("6digito", AS11);	    matrizAS.put("6/", AS4);	matrizAS.put("6,", AS11);  matrizAS.put("6+", AS11);  matrizAS.put("6*():{}", AS11);  matrizAS.put("6=", AS11);   matrizAS.put("6-", AS11);  matrizAS.put("6>", AS11);    matrizAS.put("6<", AS11);    matrizAS.put("6_", AS11);  matrizAS.put("6otro", AS2);  matrizAS.put("6blanco", AS11);   matrizAS.put("6saltotab", AS11);    matrizAS.put("6.", AS11);
		matrizAS.put("7letra", AS4);     matrizAS.put("7digito", AS4);		matrizAS.put("7/", AS4);	matrizAS.put("7,", AS4);   matrizAS.put("7+", AS4);   matrizAS.put("7*():{}", AS4);   matrizAS.put("7=", AS4);    matrizAS.put("7-", AS4);   matrizAS.put("7>", AS4);     matrizAS.put("7<", AS4);     matrizAS.put("7_", AS4);   matrizAS.put("7otro", AS2);  matrizAS.put("7blanco", AS4);    matrizAS.put("7saltotab", AS13);    matrizAS.put("7.", AS4);
	}
	
	
	private String getKeyMatrizTE(Character c){
		//Identifica el caracter entrada, y devuelve el estado siguiente segun el estado actual + tipo de caracter
		if (Character.isLetter(c)){
			return "" + estadoActual + "letra"; 
		}
		if (Character.isDigit(c)){
			return ("" + estadoActual + "digito"); 
		}
		if (c.charValue() == '/'){
			return ("" + estadoActual + "/"); 
		}
		if (c.charValue() == ','){
			return ("" + estadoActual + ","); 
		}
		if (c.charValue() == '+'){
			return ("" + estadoActual + "+"); 
		}
		if ((c.charValue() == '*') || (c.charValue() == '(') || (c.charValue() == ')') || (c.charValue() == ':') || (c.charValue() == '{') || (c.charValue() == '}')){
			return ("" + estadoActual + "*():{}"); 
		}
		if (c.charValue() == '='){
			return ("" + estadoActual + "="); 
		}
		if (c.charValue() == '-'){
			return ("" + estadoActual + "-"); 
		}
		if (c.charValue() == '>'){
			return ("" + estadoActual + ">"); 
		}
		if (c.charValue() == '<'){
			return ("" + estadoActual + "<"); 
		}
		if (c.charValue() == '_'){
			return ("" + estadoActual + "_"); 
		}
		if (c.charValue() == ' '){
			return ("" + estadoActual + "blanco"); 
		}
		if ((c.charValue() == '\t') || (c.charValue() == '\n')){
			return ("" + estadoActual + "tab"); 
		}
		if (c.charValue() == '.'){
			return ("" + estadoActual + "."); 
		}
		return ("" + estadoActual + "otro"); 
	}
	
	public Token generarToken() {
		//Iterar el codigo de a un caracter e ir avanzando en la matriz TE
		while (iterator < entrada.length()){
			caracterActual = getCaracterEntrada(); 
			//Actualizar el estado actual segun la matrizTE
			String keyMatriz = this.getKeyMatrizTE(caracterActual);
			if (keyMatriz != null){
				Integer idToken = matrizAS.get(keyMatriz).ejecutarAccion(this, stringActual , caracterActual); 
				estadoActual = matrizTE.get(keyMatriz); 
				if (idToken == -2) //LLego a un estado de error. 
					estadoActual = -2; 
				if (estadoActual == -1){ //LLego a estado final, reconociendo un token
					listaTokens.add(token); 
					estadoActual = 0; 
					stringActual.delete(0, stringActual.length());
					avanzarIterator();
					return token; 
				}
				else if(estadoActual == -2){
					estadoActual = 0; 
					stringActual.delete(0, stringActual.length()); 
				}
			}
			avanzarIterator();
		}
		return null; 

	}

	private int getNroSimbolo(char c) {
		switch (c) {
			case '{': {
				return 2;
			}
			case '}': {
				return 3;
			}
			case '(': {
				return 4;
			}
			case ')': {
				return 5;
			}
			case ',': {
				return 6;
			}
			case '+': {
				return 7;
			}
			case '-': {
				return 8;
			}
			case '=': {
				return 9;
			}
			case '/': {
				return 10;
			}
			case '>': {
				return 11;
			}
			case '<': {
				return 12;
			}
			case '*': {
				return 13;
			}
			case '\n': {
				return 14;
			}
			case ' ': {
				return 15;
			}
			case '\t': {
				return 16;
			}
		}
		if (Character.isLetter(c))
			return 0;
		if (Character.isDigit(c))
			return 1;
		return 17; // Otros
	}
	
	public int getLineaActual(){
		return lineaActual; 
	}
	
	public void addLineaActual(){
		lineaActual++; 
	}

	public void agregarError(String string) {
				
	}

	public Short esPalabraReservada(String c){
		return palabrasReservadas.get(c); 
	}




}
