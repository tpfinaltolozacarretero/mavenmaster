package edu.unicen.trazas.bean.compilador;

import java.util.Hashtable;
import java.util.Set;

public class TablaDeSimbolos {
	
	private Hashtable<String, Token> tabla; //La key corresponde al nombre (String) del identificador.
	

	public TablaDeSimbolos() {
		tabla = new Hashtable<String, Token>();
	}

	public void agregarVariable(Token t){
		tabla.put(t.getIndice(), t); 
	}
	
	public Token existeIndice(String i){
		return tabla.get(i); 
	}
		
	public void setLexema(int indice, String lexema) {
		tabla.get(indice).setLexema(lexema);
	}

	public String getLexema(int indice) {
		if (indice != -1)
			return tabla.get(indice).getLexema();
		else
			return " ";
	}

	public String imprmirTablaSimbolos() {
		String salida = "---TABLA DE SIMBOLOS--- \n";
		Set<String> keys = tabla.keySet();
		for (String key : keys) {
			Token r = tabla.get(key);
			salida += "INDICE:" + key + " - ID:" + r.getId() + " - LEXEMA:" + r.getLexema();
			//+ " - TipoCte: " + r.getTipoCte() + "- USO:" + r.getUso() + " TIPO: " + r.getTipo();			
			salida += "\n";
		}
		

		return salida;
	}

}
	

