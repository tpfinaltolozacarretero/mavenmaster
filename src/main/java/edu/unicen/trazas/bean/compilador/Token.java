package edu.unicen.trazas.bean.compilador;

import java.util.HashMap;

public class Token {
	private short id;
	private String indice;
	private String lexema; 
	private Integer iterador; 
	private HashMap<String, Float> valorPorLinea; 
	
	public final static short IDENTIFICATOR = 257;
	public final static short CONSTANT = 258; 

	public final static short FOR = 262;
	
	public final static short COMP_IGUAL = 263;
	public final static short COMP_MAYOR_IGUAL = 264;
	public final static short COMP_MENOR_IGUAL = 265;
	public final static short COMP_DISTINTO = 266;
	public final static short COMP_MENOR = 60; 
	public final static short COMP_MAYOR = 62;
	public final static short FLOAT = 300; 
	public final static short INTEGER = 301;

	

	public Token() {
		this.id = -1;
		this.indice= null;
		this.lexema = null; 
		this.iterador = null; 
		this.valorPorLinea = new HashMap<>(); 
	}
	
	public void addValorPorLinea(Integer linea, Float valor){
		valorPorLinea.put(this.indice + linea, valor); 
	}
	
	public void setIterador(Integer i){
		this.iterador = i; 
	}
	
	public Integer getIterador(){
		return this.iterador; 
	}
	
	public short getId() {
		return id;
	}

	public String getLexema() {
		return lexema;
	}
	
	public String getIndice(){
		return indice;
	}
	
	public void setIndice(String indice){
		this.indice=indice;
	}
	
	public void setLexema(String l){
		this.lexema = l; 
	}
	

	public void setId(short i){
		this.id = i; 
	}
}
