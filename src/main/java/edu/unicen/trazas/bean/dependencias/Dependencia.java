package edu.unicen.trazas.bean.dependencias;

import edu.unicen.trazas.bean.compilador.*;

public abstract class Dependencia {
	
	private Token tokenIndependiente = new Token(); 
	private Token tokenDependiente = new Token(); 
	private int linea; 
	
	public Dependencia(Token tI, Token tD, int l){
		tokenDependiente = tD; 
		tokenIndependiente = tI; 
		linea = l; 
	}
	

}
