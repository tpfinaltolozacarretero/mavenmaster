package edu.unicen.trazas.bean.dependencias;

public class NodoDependencia {
	private boolean izq;
	private String iterador;
	private int linea, valorIterador;
	
	public NodoDependencia(int linea ,  String iterador , int valorIterador , boolean izq) {
		setIzq(izq);
		setIterador(iterador);
		setValorIterador(valorIterador);
		setLinea(linea);
	}

	public boolean isIzq() {
		return izq;
	}

	public void setIzq(boolean izq) {
		this.izq = izq;
	}

	public String getIterador() {
		return iterador;
	}

	public void setIterador(String iterador) {
		this.iterador = iterador;
	}

	public int getLinea() {
		return linea;
	}

	public void setLinea(int linea) {
		this.linea = linea;
	}

	public int getValorIterador() {
		return valorIterador;
	}

	public void setValorIterador(int valorIterador) {
		this.valorIterador = valorIterador;
	}
	
	
}
