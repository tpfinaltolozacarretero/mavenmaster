package edu.unicen.trazas.bean.dependencias;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import edu.unicen.trazas.bean.compilador.Token;
import edu.unicen.trazas.bean.grafo.Arco;
import edu.unicen.trazas.bean.dependencias.*;

public class TablaDeDependencias {

	
	private HashMap<String, ArrayList<Dependencia>> listaDependencias; //Key hace referencia a una variable Independiente con una Lista de variables  correspondientes a los valores de la hash

	
	public void agregarDependenciaRAW(Token tI, Token tD, int linea){
		
		
		ArrayList<Dependencia> listaDepToken = listaDependencias.get(tD.getIndice()); 
		
		if (listaDepToken == null){
			listaDepToken = new ArrayList<>(); 
			listaDepToken.add(new DependenciaRAW(tI, tD, linea));
			listaDependencias.put(tD.getIndice(), listaDepToken); 
		}
		else {
			listaDepToken.add(new DependenciaRAW(tI, tD, linea));			
		}
	}
	
	
	
/*
	
		private Map<String,List<NodoDependencia>> arreglos;
	public TablaDeDependencias(Vector<String> arreglos ) { //lineasFor son la cantidad de lineas q estan dentro de el for
		for(String a : arreglos) {
			this.arreglos.put(a, new ArrayList<NodoDependencia>());
		}
	}*/
	
	//Lo mejor es una matriz q tenga 3 estados, 0 esta a izquierda, 1 esta a derecha, y 2 esta tanto a izquierda como a derecha.
	//Las 
	
	/*public void add(String variable , int linea , String iterador , int valorIterador , boolean izq) {
		 arreglos.get(variable).add(new NodoDependencia(linea,iterador,valorIterador,izq));
	}
	 
	private List<Arco> devolverDependenciasArreglo(List<NodoDependencia> nodo) {
		List<Arco> arcos = null;		
		return arcos;
	};
	
	public List<Arco> devolverArcosDependencias() {
		List<Arco> listaArco = new ArrayList<>();
		for(String arr : arreglos.keySet()){
			
		}
		return listaArco;
	}*/
}
