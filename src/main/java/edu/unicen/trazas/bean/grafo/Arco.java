package edu.unicen.trazas.bean.grafo;

public class Arco {
	
	private int origen,destino,costo,ciclos,tipo;
	
	//El tipo es para saber si es de retroceso, etc, no se si lo vamos a usar todavia
	
	public Arco(int origen , int destino , int costo, int ciclos , int tipo) {
		this.setOrigen(origen);
		this.setDestino(destino);
		this.setCosto(costo);
		this.setCosto(0);
		this.setCiclos(ciclos);
	}

	public boolean isMayor(Arco a) {
		return (this.costo > a.getCosto());
	}
	
	public boolean isMenor(Arco a) {
		return (this.costo < a.getCosto());
	}
	
	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}

	public int getDestino() {
		return destino;
	}

	public void setDestino(int destino) {
		this.destino = destino;
	}

	public int getCosto() {
		return costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getCiclos() {
		return ciclos;
	}

	public void setCiclos(int ciclos) {
		this.ciclos = ciclos;
	}
}
