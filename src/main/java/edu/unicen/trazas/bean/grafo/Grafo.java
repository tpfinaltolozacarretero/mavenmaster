package edu.unicen.trazas.bean.grafo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Grafo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9057369115843314357L;
	private List<Arco> arcos; 
	private List<Integer> vertices;
	private boolean[] unidos,visitados;

	public Grafo() {
		arcos = new ArrayList<>();
		vertices = new ArrayList<>();
	}
	
	public Grafo(List<Arco> arcos , List<Integer> vertices) {
		this.arcos = arcos;
		this.vertices = vertices;
	}
	
	public void addVertice(int v) {
		vertices.add(v);
	}
	
	public void addArco(Arco arco) {
		arcos.add(arco);
	}
	
	public void addArco(int origen, int destino, int costo, int ciclos , int tipo) {
		Arco a = new Arco(origen,destino,costo,ciclos,tipo);
		arcos.add(a);
	}
	
	public List<Arco> devolverAdyacentes(int vertice) {
		List<Arco> ady = new ArrayList<>();
		for (Arco a : arcos) {
			if (a.getOrigen() == vertice) {
				ady.add(a);
			}
		}
		return ady;		
	}
	
	//BFS con la traza mas larga
	public List<Arco> ramaMasLarga(){
		List<Arco> aux = null;
		return aux;
	}

	public List<Arco> getArcos() {
		return arcos;
	}

	public void setArcos(List<Arco> arcos) {
		this.arcos = arcos;
	}

	public List<Integer> getVertices() {
		return vertices;
	}

	public void setVertices(List<Integer> vertices) {
		this.vertices = vertices;
	}
	
	/*public List<Arco> caminoMasLargo(){
		int n = vertices.size();
		int[][] A = new int[n][n];
		int[][] P = new int[n][n];
		for(int i = 0 ; i <= n ; i++ ) {
			for(int j = 0 ; j <= n ; j++ ) {
				A[i][j] = -1;
			}
		}
		for(Arco a : arcos) {
			A[a.getOrigen()][a.getDestino()] = a.getCosto();
		}
		for(int k = 0 ; k <= n ; k++ ) {
			for(int i = 0 ; i <= n ; i++ ) {
				for(int j = 0 ; j <= n ; j++ ) {
					if ( A[i][k] + A[k][j] > A[i][j] ) {
						A[i][j] = A[i][k] + A[k][j];
						P[i][j] = k;
					}
				}
			}
		}
		return null;
	}*/
	
	
	
	private void dfsBucle (int index, String traza, int bucle, List<String> soluciones, int anterior){
		 unidos[index] = true;
		 visitados[index] = true;
		 if (index == vertices.size()) { //si es ultimo vertice del for
			String trazafinal = traza;
			 
		 	if (bucle != -1){
		 		trazafinal =trazafinal.substring(0, trazafinal.length()-2);
		 		soluciones.add(trazafinal);
		 	} 
		 }
		 else {
			//Le pongo el nombre
			if (bucle != index)
				 traza+="S" + index +" + ";
			 
			for( Arco ady : devolverAdyacentes(index) ){
		
				if (!visitados[ady.getDestino()]){	
					this.dfsBucle(ady.getDestino(),traza,bucle,soluciones,index); 
				}
				else{
					 if (ady.getDestino() == index && bucle == -1)
					 {
						 bucle=index;
						 traza= traza.substring(0, traza.length()-3) + "*N + ";
						 boolean dividido = false;
						 if(ady.getCiclos()>1) {
							 dividido=true;
							 traza=traza.substring(0, traza.length()-4)+"(N/" + ady.getCiclos() + ") + ";
						 }
					
						 this.dfsBucle(ady.getDestino(),traza,bucle,soluciones,index);
						 bucle=-1;
						 if (!dividido)
							 traza=traza.substring(0, traza.length()-5)+ " + ";
						 else
							 traza=traza.substring(0, traza.length()-9)+ " + ";
					 }
				}
			}
			 
		 }
		 if (anterior != index)
			 visitados[index] = false;
		
	}
	
	private int buscarCiclo(List<Integer> costo,List<Integer> traza,int index,int costoInicial){		
		int j=traza.lastIndexOf(index)-1;
		int  contador=costoInicial;
		for (int i=traza.indexOf(index) ; i < j; i++){
			contador+=costo.get(i);
		}
		return contador;
	}
	
	private void dfsCiclo (int index, List<Integer> traza, List<String>soluciones, List<Integer> costo,int ciclo,int costoCiclo,int anterior){
		visitados[index] = true;
		unidos[index] = true;
		if (index == vertices.size() )
		{
			if (ciclo != -1){
				//soluciones.add(armarTraza(traza,ciclo,costoCiclo));
			}
		}
		else{
			if (traza.indexOf(index) == -1){  // Pretenezco a la traza
				traza.add(index);
			}
			for( Arco ady : devolverAdyacentes(index) ){ // Por cada adyacente 
				if (!visitados[ady.getDestino()]){
					costo.add(ady.getCiclos());
					this.dfsCiclo(ady.getDestino(),traza,soluciones,costo,ciclo,costoCiclo,index);
					costo.remove(costo.size()-1);
					}
				else{
					if(	ady.getDestino() != index && ciclo == -1 ){
						ciclo=ady.getDestino();
						traza.add(ady.getDestino());
						costoCiclo= buscarCiclo(costo,traza,ciclo,ady.getCiclos());
						for(int primeroCiclo = traza.indexOf(ady.getDestino()); primeroCiclo < traza.size(); primeroCiclo++){
							dfsCiclo(traza.get(primeroCiclo),traza,soluciones,costo,ciclo,costoCiclo,traza.get(primeroCiclo));
						}
						ciclo=-1;
						traza.remove(traza.size()-1);
					}
					
				}
			}
			if(index != anterior){
				traza.remove(traza.size()-1);
			}
		}
		if(index != anterior){
			visitados[index] = false;
		}
		
	};
	public List<String> getTrazas(){
		List<String> solucion = new ArrayList<>();
		List<String> traza = new ArrayList<>();
		int coste=0;
		int bucle = -1;
		unidos = new boolean[vertices.size()];
		visitados= new boolean[vertices.size()];
		for (Integer v : vertices){
			if(!unidos[v]){
				this.dfsBucle(v,"",-1,solucion,-1);
				this.dfsCiclo(v,traza,-1,0,solucion,-1);
			}
		}
		//this.grillaTrazas=[];
		/*var text="";
		for (var i in this.soluciones)
		{
			text=this.soluciones[i];
			for (var j in this.tiempoSentencia)
			{
				var sentencia="S"+Number(Number(1)+Number(j));
				text =text.replace(sentencia, this.tiempoSentencia[j]);
			}
			text =text.replace("N", numeroIteraciones);
			this.grillaTrazas.push({traza:this.soluciones[i], calculo:text, resultado:Number(eval(text).toFixed(2))});
		}*/
	}
}
