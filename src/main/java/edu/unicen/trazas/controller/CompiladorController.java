package edu.unicen.trazas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.unicen.trazas.bean.grafo.Grafo;
import edu.unicen.trazas.service.CompiladorService;

@RestController
@RequestMapping("compilador")
public class CompiladorController {

	@Autowired
	CompiladorService compilador;
	
	@RequestMapping(value="/grafo", method=RequestMethod.POST) 
	public ResponseEntity<Grafo> getGrafo(
			@RequestBody String programa){
		return ResponseEntity.ok(compilador.getGrafo(programa));
	}
}
