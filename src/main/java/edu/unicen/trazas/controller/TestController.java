package edu.unicen.trazas.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.unicen.trazas.bean.grafo.Grafo;
import edu.unicen.trazas.service.CompiladorService;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired //Delego a spring boot  el manejo de la clase pero la implementacion la defino yo
    private CompiladorService compilador;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getMethod() throws IOException {
    	return ResponseEntity.ok("Hola mundo!");
    }

    @RequestMapping(value="/otro", method = RequestMethod.GET)
    public ResponseEntity<Grafo> getMethod2() throws IOException {
    	return ResponseEntity.ok(compilador.getGrafo("Sisi"));
    }

}
