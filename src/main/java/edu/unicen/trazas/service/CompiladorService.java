package edu.unicen.trazas.service;

import org.springframework.stereotype.Component;

import edu.unicen.trazas.bean.grafo.Arco;
import edu.unicen.trazas.bean.grafo.Grafo;

@Component //Cuando digo esto la instancia es Singletone, es decir me define una sola instancia y la mantengo
public class CompiladorService {
	
	public Grafo getGrafo(String programa) {
		Grafo g = new Grafo();
		Arco a = new Arco(0,1,50,0,0);
		g.addArco(a);
		/*String resultado = "Origen: " + Integer.toString(a.getOrigen()) 
						+ "Detino: " +  Integer.toString(a.getDestino())
						+ "Costo: " + Integer.toString(a.getCosto())
						+ "Ciclos: " + Integer.toString(a.getCiclos())
						+ "Tipo: " + Integer.toString(a.getTipo());
		*/
		return g;
	}
}
